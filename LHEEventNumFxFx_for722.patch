commit a2848f1bafcc5f907f97ac2b10b9187100e3e758
Author: Andreas Papaefstathiou <andreas.papaef@gmail.com>
Date:   Thu Jan 13 18:46:44 2022 -0500

    LHE event numbers for FxFx

diff --git a/MatrixElement/FxFx/FxFxEventHandler.cc b/MatrixElement/FxFx/FxFxEventHandler.cc
index fcdc4ec..b195b40 100644
--- a/MatrixElement/FxFx/FxFxEventHandler.cc
+++ b/MatrixElement/FxFx/FxFxEventHandler.cc
@@ -249,8 +249,15 @@ EventPtr FxFxEventHandler::generateEvent() {
 
       theLastXComb = currentReader()->getXComb();
 
-      currentEvent(new_ptr(Event(lastParticles(), this, generator()->runName(),
-				 generator()->currentEventNumber(), weight*fact)));
+      //whether to use the LHE event number or not for the event identification
+      if(UseLHEEvent==0 || currentReader()->LHEEventNum() == -1) { 
+	currentEvent(new_ptr(Event(lastParticles(), this, generator()->runName(),
+                                 generator()->currentEventNumber(), weight*fact )));
+      }
+      else if(UseLHEEvent==1 && currentReader()->LHEEventNum() != -1) {
+	currentEvent(new_ptr(Event(lastParticles(), this, generator()->runName(),
+				  currentReader()->LHEEventNum(), weight*fact )));
+      }
       currentEvent()->optionalWeights() = currentReader()->optionalEventWeights();
      // normalize the optional weights
       for(map<string,double>::iterator it = currentEvent()->optionalWeights().begin();
@@ -543,14 +550,14 @@ int FxFxEventHandler::ntriesinternal() const {
 void FxFxEventHandler::persistentOutput(PersistentOStream & os) const {
   os << stats << histStats << theReaders << theSelector
      << oenum(theWeightOption) << theUnitTolerance << theCurrentReader << warnPNum
-     << theNormWeight;
+     << theNormWeight << UseLHEEvent;
 
 }
 
 void FxFxEventHandler::persistentInput(PersistentIStream & is, int) {
   is >> stats >> histStats >> theReaders >> theSelector
      >> ienum(theWeightOption) >> theUnitTolerance >> theCurrentReader >> warnPNum
-     >> theNormWeight;
+     >> theNormWeight >> UseLHEEvent;;
 }
 
 ClassDescription<FxFxEventHandler>
@@ -644,6 +651,20 @@ void FxFxEventHandler::Init() {
      "Normalize the weights to the max cross section in pb",
      1);
 
+    static Switch<FxFxEventHandler,unsigned int> interfaceEventNumbering
+    ("EventNumbering",
+     "How to number the events",
+     &FxFxEventHandler::UseLHEEvent, 0, false, false);
+  static SwitchOption interfaceEventNumberingIncremental
+    (interfaceEventNumbering,
+     "Incremental",
+     "Standard incremental numbering (i.e. as they are generated)",
+     0);
+  static SwitchOption interfaceEventNumberingLHE
+    (interfaceEventNumbering,
+     "LHE",
+     "Corresponding to the LHE event number",
+     1);
 
   interfaceFxFxReaders.rank(10);
   interfaceWeightOption.rank(9);
diff --git a/MatrixElement/FxFx/FxFxEventHandler.h b/MatrixElement/FxFx/FxFxEventHandler.h
index f6f1582..cb92250 100644
--- a/MatrixElement/FxFx/FxFxEventHandler.h
+++ b/MatrixElement/FxFx/FxFxEventHandler.h
@@ -72,7 +72,7 @@ public:
    * The default constructor.
    */
   FxFxEventHandler()
-    : theWeightOption(unitweight), theUnitTolerance(1.0e-6), warnPNum(true), theNormWeight(0)
+    : theWeightOption(unitweight), theUnitTolerance(1.0e-6), warnPNum(true), theNormWeight(0), UseLHEEvent(0)
   {
     selector().tolerance(unitTolerance());
   }
@@ -377,6 +377,11 @@ private:
    */
   unsigned int theNormWeight;
 
+  /** 
+   * How to number the events
+   */
+  unsigned int UseLHEEvent;
+
 
 public:
 
diff --git a/MatrixElement/FxFx/FxFxFileReader.cc b/MatrixElement/FxFx/FxFxFileReader.cc
index b956ba6..63ce2a4 100644
--- a/MatrixElement/FxFx/FxFxFileReader.cc
+++ b/MatrixElement/FxFx/FxFxFileReader.cc
@@ -707,6 +707,9 @@ bool FxFxFileReader::doReadEvent() {
     } 
   }
 
+  LHEeventnum = -1; // set the LHEeventnum to -1, this will be the default if the tag <event num> is not found
+
+
  // Now read any additional comments and named weights.
   // read until the end of rwgt is found
   bool readingWeights = false, readingaMCFast = false, readingMG5ClusInfo = false;
@@ -767,6 +770,20 @@ bool FxFxFileReader::doReadEvent() {
       erase_substr(mg5clusinfo,str_newline);
       optionalWeights[mg5clusinfo.c_str()] = -222; //for the mg5 scale info weights we give them a weight -222 for future identification
     }
+
+    //the event num tag
+    if(cfile.find("<event num")) {
+      string hs = cfile.getline();
+      string startDEL = "<event num"; //starting delimiter
+      string stopDEL = "</event num>"; //end delimiter
+      unsigned firstLim = hs.find(startDEL);
+      string LHEeventnum_str = hs.substr(firstLim);
+      erase_substr(LHEeventnum_str,stopDEL);
+      erase_substr(LHEeventnum_str,startDEL);
+      string str_arrow = ">";
+      erase_substr(LHEeventnum_str,str_arrow);
+      LHEeventnum =  std::stol(LHEeventnum_str, nullptr, 10); 
+    }
     
     //store MG5 clustering information 
     if(cfile.find("<scales")) {
diff --git a/MatrixElement/FxFx/FxFxReader.cc b/MatrixElement/FxFx/FxFxReader.cc
index 3dafe2c..810e8bf 100644
--- a/MatrixElement/FxFx/FxFxReader.cc
+++ b/MatrixElement/FxFx/FxFxReader.cc
@@ -928,6 +928,7 @@ void FxFxReader::createParticles() {
       for(unsigned int ic1=0;ic1<col.size();++ic1) {
 	bool matched=false;
 	for(unsigned int iy=0;iy<external.size();++iy) {
+	  if(iy==ix) continue;
 	  vector<tcColinePtr> col2;
 	  if(hepeup.ISTUP[particleIndex(external[iy])-1]<0) {
 	    if(external[iy]->colourInfo()->colourLines().empty()) continue;
@@ -949,20 +950,21 @@ void FxFxReader::createParticles() {
       }
     }
     if(!anti.empty()) {
-      for(unsigned int ic1=0;ic1<col.size();++ic1) {
+      for(unsigned int ic1=0;ic1<anti.size();++ic1) {
 	bool matched=false;
 	for(unsigned int iy=0;iy<external.size();++iy) {
+	  if(iy==ix) continue;
 	  vector<tcColinePtr> anti2;
 	  if(hepeup.ISTUP[particleIndex(external[iy])-1]<0) {
-	    if(external[iy]->colourInfo()->colourLines().empty()) continue;
+	    if(external[iy]->colourInfo()->antiColourLines().empty()) continue;
 	    anti2 = external[iy]->colourInfo()->antiColourLines();
 	  } 
 	  else if(hepeup.ISTUP[particleIndex(external[iy])-1]>0) {
-	    if(external[iy]->colourInfo()->antiColourLines().empty()) continue;
+	    if(external[iy]->colourInfo()->colourLines().empty()) continue;
 	    anti2 = external[iy]->colourInfo()->colourLines();
 	  }
 	  for(unsigned int ic2=0;ic2<anti2.size();++ic2) {
-	    if(col[ic1]==anti2[ic2]) {
+	    if(anti[ic1]==anti2[ic2]) {
 	      matched=true;
 	      break;
 	    }
@@ -1185,6 +1187,7 @@ void FxFxReader::cacheEvent() const {
   pos = mwrite(pos, hepeup.SPINUP[0], hepeup.NUP);
   pos = mwrite(pos, lastweight);
   pos = mwrite(pos, optionalWeights);
+  pos = mwrite(pos, LHEeventnum);
   for(size_t ff = 0; ff < optionalWeightsNames.size(); ff++) {
     pos = mwrite(pos, optionalWeightsNames[ff]);
   }
@@ -1219,7 +1222,7 @@ bool FxFxReader::uncacheEvent() {
   pos = mread(pos, hepeup.MOTHUP[0], hepeup.NUP);
   hepeup.ICOLUP.resize(hepeup.NUP);
   pos = mread(pos, hepeup.ICOLUP[0], hepeup.NUP);
-  hepeup.PUP.resize(hepeup.NUP, vector<double>(5));
+  hepeup.PUP.resize(hepeup.NUP);
   for ( int i = 0; i < hepeup.NUP; ++i ) 
     pos = mread(pos, hepeup.PUP[i][0], 5);
   hepeup.VTIMUP.resize(hepeup.NUP);
@@ -1234,6 +1237,7 @@ bool FxFxReader::uncacheEvent() {
   pos = mread(pos, optionalnpLO);
   pos = mread(pos, optionalnpNLO);
   pos = mread(pos, preweight);
+  pos = mread(pos, LHEeventnum);
 
   // If we are skipping, we do not have to do anything else.
   if ( skipping ) return true;
@@ -1263,7 +1267,7 @@ void FxFxReader::persistentOutput(PersistentOStream & os) const {
      << thePartonBinInstances
      << theBeams << theIncoming << theOutgoing << theIntermediates
      << reweights << preweights << preweight << reweightPDF << doInitPDFs
-     << theLastXComb << theMaxMultCKKW << theMinMultCKKW << lastweight << optionalWeights << optionalnpLO << optionalnpNLO
+     << theLastXComb << theMaxMultCKKW << theMinMultCKKW << lastweight << optionalWeights << optionalnpLO << optionalnpNLO << LHEeventnum
      << maxFactor << ounit(weightScale, picobarn) << xSecWeights << maxWeights
      << theMomentumTreatment << useWeightWarnings << theReOpenAllowed
      << theIncludeSpin;
@@ -1284,7 +1288,7 @@ void FxFxReader::persistentInput(PersistentIStream & is, int) {
      >> thePartonBinInstances
      >> theBeams >> theIncoming >> theOutgoing >> theIntermediates
      >> reweights >> preweights >> preweight >> reweightPDF >> doInitPDFs
-     >> theLastXComb >> theMaxMultCKKW >> theMinMultCKKW >> lastweight >> optionalWeights >> optionalnpLO >> optionalnpNLO
+     >> theLastXComb >> theMaxMultCKKW >> theMinMultCKKW >> lastweight >> optionalWeights >> optionalnpLO >> optionalnpNLO >> LHEeventnum
      >> maxFactor >> iunit(weightScale, picobarn) >> xSecWeights >> maxWeights
      >> theMomentumTreatment >> useWeightWarnings >> theReOpenAllowed
      >> theIncludeSpin;
diff --git a/MatrixElement/FxFx/FxFxReader.h b/MatrixElement/FxFx/FxFxReader.h
index 23ddbd3..e31b917 100644
--- a/MatrixElement/FxFx/FxFxReader.h
+++ b/MatrixElement/FxFx/FxFxReader.h
@@ -290,6 +290,11 @@ public:
    */
   const map<string,double>& optionalEventWeights() const { return optionalWeights; }
 
+  /** 
+   * Return the Les Houches event number associated with the current event
+   */
+  const long& LHEEventNum() const { return LHEeventnum; }
+
   /**
    * Return the optional npLO and npNLO
    */
@@ -834,6 +839,11 @@ protected:
    * The optional weights associated to the last read events.
    */
   map<string,double> optionalWeights;
+  
+  /**
+   * The event number
+   */
+  long LHEeventnum;
 
    /**
    * If the maximum cross section of this reader has been increased
